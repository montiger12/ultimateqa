﻿using Atata;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.IO;

namespace UltimateQA
{
    public class UQATests : UITestFixture
    {
        public UQATests(string driverAlias)
           : base(driverAlias)
        {
        }

        [Test]
        [Retry(2)]
        [Order(1)]
        public void PageLoad()
        {
            //load the URL
            var mainPage = Go.To<MainPage>();

            //Log a message and screenshot before maximize
            LogMessage("Page Loaded", "PageLoad");

            //Maximize the Window
            AtataContext.Current.Driver.Manage().Window.Maximize();

            //Log a message and screenshot 
            LogMessage("Maximized Window", "MaximizedWindow");

            //Assertion of Title and main heading
            mainPage.PageTitle.Should.Equal("Automation Practice - Ultimate QA");
            mainPage.Heading.Should.Equal("Automation Practice");

            //Log a message and screenshot
            LogMessage("Title and heading Displayed as expected", "TitleDisplayed");

        }

        [Test]
        [Retry(2)]
        [Order(2)]
        public void BuyPackage()
        {
            //load URL
            var mainPage = Go.To<MainPage>();
            var driver = AtataContext.Current.Driver;
            driver.Manage().Window.Maximize();

            //click on pricing plans link
            PricingPage pricePage = mainPage.Pricing.ClickAndGo();

            driver.ExecuteScript("window.scrollTo(0,400)");

            //Check for the FAQ text to confirm page
            pricePage.FAQ.Should.BeVisible();           

            //Log a message and screenshot
            LogMessage("Price Page Loaded", "PricePageLoaded");

            //Hover over the purchase button so it changes colour
            pricePage.Purchase.Hover().Wait(3);

            //Log a message and screenshot
            LogMessage("Basic Purchase Button Hover", "HoverPurchaseButton");

            //Click on Purchase button
            pricePage.Purchase.Click().Wait(3);

            if (driver.Url.Contains("#top"))
            {
                //Log a message and screenshot
                LogMessage("Basic Purchase Click", "ClickPurchaseButton");
            }
            else
            {
                //Log a message and screenshot
                Assert.Fail("url does not contain #top");
                LogError($"UrlIncorrect: {driver.Url} ", "URLScreen");
            }           
        }

        [Test]
        [Retry(2)]
        [Order(3)]
        public void LoginLogout()
        {
            //Load Url
            var mainPage = Go.To<MainPage>();

            AtataContext.Current.Driver.Manage().Window.Maximize();

            //click on login link
            LoginPage loginPage = mainPage.Login.ClickAndGo().Wait(5);

            //Log a message and screenshot
            LogMessage("LoginPage Loaded", "LoginPageLoaded");

            //Fill in credentials
            loginPage.EmailAdd.Scope.SendKeys("montiger12@hotmail.com");
            Thread.Sleep(500);
            loginPage.Password.Scope.SendKeys("test123");
            Thread.Sleep(500);
            LogoutPage logoutPage = loginPage.Submit.ClickAndGo<LogoutPage>();

            //if the Captcha frame pops up, use this method to solve it
            if (loginPage.CaptchaFrame.Exists())
            {
                LogMessage("Captcha Found", "CaptchaImage");
                FramePage framePage = loginPage.CaptchaFrame.SwitchTo<FramePage>();

                logoutPage = framePage.SolveCaptcha(framePage);
            }

            //Assert My Dashboard text
            logoutPage.MyDashboard.Should.Exist().Wait(5);

            //Log a message and screenshot
            LogMessage("Login Succeeded", "LoginSuccess");

            //Logout
            logoutPage.Avatar.Click();

            LogMessage("Login Succeeded", "LoginSuccess");

            CoursesPage courses = logoutPage.SignOut.ClickAndGo().Wait(3);

            //Assert sign in text
            courses.SignIn.Should.Exist();

            //Log a message and screenshot
            LogMessage("Logout Succeeded", "LogoutSuccess");

        }

        [Test]
        [Retry(2)]
        [Order(4)]
        public void FillInForms()
        {

            //load URL
            var mainPage = Go.To<MainPage>();
            AtataContext.Current.Driver.Manage().Window.Maximize();

            //Click on Forms Link
            FormsPage formsPage = mainPage.FillForms.ClickAndGo();

            //Log a message and screenshot
            LogMessage("FormsPage Loaded", "FormsPageLoaded");

            //Fill in the form
            formsPage = FillInForm0(formsPage);

            //If there is an error message, try fill in the form again
            do
            {
                if (formsPage.ContactName0.Exists())
                {
                    formsPage = FillInForm0(formsPage).Wait(3);
                }
            }
            while (formsPage.ErrorText.Exists());

            //Assert Form 0 success text
            formsPage.MessageText.Should.Exist();
            formsPage.MessageText.Should.Equal("Form filled out successfully");

            //Log a message and screenshot
            LogMessage("Form0 filled out successfully", "Form0Success");

            //Fill in Form 1
            formsPage.ContactName1.Set("Bob Smith");
            formsPage.Message1.Set("Bob Smith says hi");

            //get the text from the sum 
            formsPage.Sum.Get(out string sum);

            //get the result from the sum
            int result = findSum(sum);

            //fill in answer for the sum
            formsPage.SumAnswer.Set(result.ToString());
            formsPage.Submit1.Click().Wait(3);

            //Assert Form 1 success
            formsPage.Form1Success.Should.Exist();
            formsPage.Form1Success.Should.Equal("Success");

            //Log a message and screenshot
            LogMessage("Form1 filled out successfully", "Form1Success");

        }


        #region Helpers

        //Methods to write info and screenshots to Extent Report
        public void LogMessage(string message, string screenshot)
        {
            AtataContext.Current.Log.Screenshot(screenshot);
            _testing.Info(message);
            _testing.AddScreenCaptureFromPath(Path.Combine(filepath, $"Screenshots", $"{driverAlias} - {screenshot} - {tick}.png"));
        }

        public void LogError(string message, string screenshot)
        {
            AtataContext.Current.Log.Screenshot(screenshot);
            _testing.Error(message);
            _testing.AddScreenCaptureFromPath(Path.Combine(filepath , $"Screenshots" , $"{driverAlias} - {screenshot} - {tick}.png"));
        }

        //Method to fill in Form0 of the Forms test
        public FormsPage FillInForm0(FormsPage formsPage)
        {
            formsPage.ContactName0.Clear();
            formsPage.Message0.Clear();

            formsPage.ContactName0.Set("Jane Doe");
            formsPage.Message0.Set("Jane Doe has a query");
            formsPage = formsPage.Submit0.Click();

            return formsPage;
        }

        //Doing the calculation for Form 1 in the forms test
        static int findSum(string str)
        {
            // A temporary string  
            string temp = "";

            // holds sum of all numbers 
            // present in the string  
            int sum = 0;

            // read each character in input string  
            for (int i = 0; i < str.Length; i++)
            {
                char ch = str[i];

                // if current character is a digit  
                if (char.IsDigit(ch))
                    temp += ch;

                // if current character is an alphabet  
                else
                {

                    // increment sum by number found earlier  
                    // (if any)  
                    sum += int.Parse(temp);

                    // reset temporary string to empty  
                    temp = "0";
                }
            }

            // atoi(temp.c_str()) takes care of trailing  
            // numbers  
            return sum + int.Parse(temp);
        }

        #endregion
    }
}
