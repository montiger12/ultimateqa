﻿using Atata;

namespace UltimateQA
{
    using _ = FramePage;

    public class FramePage : Page<_>
    {
        [FindById("solver-button")]
        public Button<LogoutPage, _> Solve { get; set; }

        [FindByClass("rc-audiochallenge-error-message")]
        public Text<_> Error { get; set; }

        //Method to help solve Captcha using Buster Extension
        public LogoutPage SolveCaptcha(FramePage framePage)
        {
            LogoutPage logoutPage = new LogoutPage();

            framePage.Solve.Click().Wait(5);

            do
            {
                logoutPage = framePage.Solve.ClickAndGo().Wait(5);
            }
            while (!logoutPage.MyDashboard.Exists());

            return logoutPage;
        }
    }
}
