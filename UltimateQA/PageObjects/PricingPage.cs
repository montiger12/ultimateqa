﻿using Atata;


namespace UltimateQA
{
    using _ = PricingPage;

    public class PricingPage : Page<_>
    {

        [FindByClass(TermMatch.Contains, "et_pb_pricing_table_1")]
        [Term("Purchase")]
        //[ScrollDown()]
        public Link<_> Purchase { get; set; }

        [FindByClass("et_pb_text_inner")]
        [Term("Frequently Asked Questions")]
        //[ScrollUsingMoveToElement]
        public H2<_> FAQ { get; set; }

        [Term("Pick a Plan")]
        public H1<_> MainHeading { get; set; }
    }
}
