﻿using Atata;


namespace UltimateQA
{
    using _ = LoginPage;
    [Url("https://courses.ultimateqa.com/users/sign_in")]
    public class LoginPage : Page<_>
    {
        [FindById("user[email]")]
        public EmailInput<_> EmailAdd { get; set; }

        [FindById("user[password]")]
        public PasswordInput<_> Password { get; set; }

        [FindById("user[remember_me]")]
        public CheckBox<_> RememberMe { get; set; }

        [FindByValue("Forgot Password?")]
        public Clickable<_> ForgotPassword { get; set; }

        [FindByValue("Sign in")]
        public Clickable<LogoutPage, _> Submit { get; set; }


        [FindByTitle("recaptcha challenge")]
        public Frame<_> CaptchaFrame { get; set; }

    }
}
