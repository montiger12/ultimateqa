﻿using Atata;


namespace UltimateQA
{
    using _ = FormsPage;

    public class FormsPage : Page<_>
    {
        [FindById("et_pb_contact_name_0")]
        public TextInput<_> ContactName0 { get; set; }

        [FindById("et_pb_contact_name_1")]
        public TextInput<_> ContactName1 { get; set; }

        [FindById("et_pb_contact_message_0")]
        public TextArea<_> Message0 { get; set; }

        [FindById("et_pb_contact_message_1")]
        public TextArea<_> Message1 { get; set; }

        [FindById("et_pb_contact_form_0")]
        public Button<_> Submit0 { get; set; }

        [FindById("et_pb_contact_form_1")]
        public Button<_> Submit1 { get; set; }

        [FindByClass("et_pb_contact_captcha_question")]
        public Text<_> Sum { get; set; }

        [FindByClass("input et_pb_contact_captcha")]
        public TextInput<_> SumAnswer { get; set; }

        [FindByClass("et_pb_contact_error_text")]
        public Text<_> ErrorText { get; set; }

        [FindByClass("et-pb-contact-message")]
        public Text<_> MessageText { get; set; }
        
        [FindById("et_pb_contact_form_1")]
        [Term("Success")]
        public Text<_> Form1Success { get; set; }

    }
}
