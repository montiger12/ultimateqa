﻿using Atata;


namespace UltimateQA
{
    using _ = LogoutPage;

    [Url("https://courses.ultimateqa.com/collections")]
    public class LogoutPage : Page<_>
    {

        [Term("Sign Out")]
        public Link<CoursesPage, _> SignOut { get; set; }

        [FindByClass("header__nav-item")]
        [Term("My Dashboard")]      
        public Link<_> MyDashboard { get; set; }

        [FindByClass("header__user-avatar")]
        public Clickable<_> Avatar { get; set; }

    }
}


