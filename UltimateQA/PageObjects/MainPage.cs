﻿using Atata;

namespace UltimateQA
{
    using _ = MainPage;
    
    
    public class MainPage : Page<_>
    {
        
        public H1<_> Heading { get; set; }
        
        [Term("Login automation")]
        [PressEscape]
        public Link<LoginPage, _> Login { get; set; }

        [Term("Fill out forms")]
        public Link<FormsPage, _> FillForms { get; set; }

        [Term("Fake Pricing Page")]
        public Link<PricingPage, _> Pricing { get; set; }

    }
}
