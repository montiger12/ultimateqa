﻿using Atata;
using AventStack.ExtentReports;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;

namespace UltimateQA
{
    //[TestFixture(DriverAliases.Chrome)]
    //[TestFixture(DriverAliases.Firefox)]
    [TestFixture(DriverAliases.Remote)]
    public abstract class UITestFixture : SetupFixture
    {
        public ExtentTest _testing;
        protected UITestFixture(string driverAlias)
        {
            this.driverAlias = driverAlias;

        }

        [SetUp]
        public void SetUp()
        {
            //setting up the webDriver options
            ChromeOptions options = new ChromeOptions();
            options.AddArgument($"--user-agent={RandomUserAgent()}");
            options.AddAdditionalCapability("useAutomationExtension", false);
            options.AddArguments(new List<string>() { "enable-automation"});
            options.AddUserProfilePreference("credentials_enable_service", false);
            //options.AddExtension(Directory.GetParent(filepath) + "\\Extension\\1.0.1_0.crx");

            //FirefoxProfile ffp = new FirefoxProfile();
            //ffp.SetPreference("general.useragent.override", $"--user-agent={RandomUserAgent()}");
            //ffp.AddExtension(Directory.GetParent(filepath) + "\\Extension\\extension.xpi");
            FirefoxOptions ffo = new FirefoxOptions();// { Profile = ffp };
            ffo.SetPreference("general.useragent.override", $"--user-agent={RandomUserAgent()}");                      

            var ac = AtataContext.Configure();

            if (driverAlias == "chrome")
            {
                ac.UseChrome().
                    WithOptions(options);
            }
            else if (driverAlias == "firefox")
            {
                ac.UseFirefox()
                    .WithOptions(ffo);

            }
            else
            {
                
                options.AddArgument("--headless");
                options.AddArgument("--no-sandbox");
                options.AddArgument("--proxy-server='direct://'");
                options.AddArgument("--proxy-bypass-list=*");

                ac.UseRemoteDriver().
                    WithOptions(options).
                    WithRemoteAddress("http://selenium__standalone-chrome:4444/wd/hub/");
            }

            ac.UseTestName(() => $"[{driverAlias}]{TestContext.CurrentContext.Test.Name}");
            ac.UseBaseUrl("https://www.ultimateqa.com/automation/");
            ac.UseCulture("en-us");
            ac.AddScreenshotFileSaving().
                WithFolderPath(filepath + "\\Screenshots\\").
                WithFileName(screenshotInfo => $"{driverAlias} - {screenshotInfo.Title?.Append($" - {tick}")}");
            ac.Build();

            AtataContext.Current.Driver.Manage().Cookies.DeleteAllCookies();

            _testing = _extent.CreateTest($"{TestContext.CurrentContext.Test.Name} - [{driverAlias}] - {today}");
            _testing.Log(Status.Info, "Test Starting");
        }

        //method to randomly get a user Agent from a list to use in tests
        public string RandomUserAgent()
        {
            List<string> userAgents = new List<string>()
            {
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 83.0.4103.116 Safari / 537.36",
                "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 83.0.4103.116 Safari / 537.36",
                "Mozilla / 5.0(Windows NT 10.0) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 83.0.4103.116 Safari / 537.36",
                //"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
                //"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"
            };

            Random randNum = new Random();
            int index = randNum.Next(userAgents.Count);//Returns a nonnegative random number less than the specified maximum (firstNames.Count).

            return userAgents[index];
        }


        [TearDown]
        public void TearDown()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var errorMessage = TestContext.CurrentContext.Result.Message;
            var testName = TestContext.CurrentContext.Test.Name;
            Status logstatus;
            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    _testing.Log(logstatus, $"Test ended with " + logstatus + " – " + errorMessage);
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    _testing.Log(logstatus, $"Test ended with " + logstatus);
                    break;
                default:
                    logstatus = Status.Pass;
                    _testing.Log(logstatus, $"Test ended with " + logstatus);
                    break;
            }

            AtataContext.Current.Driver.Quit();
        }
    }
}


