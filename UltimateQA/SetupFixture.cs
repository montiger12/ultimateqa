﻿using Atata;
using NUnit.Framework;
using AventStack.ExtentReports.Reporter;
using System;
using System.IO;
using AventStack.ExtentReports.Core;
using AventStack.ExtentReports;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System.Threading;
using AventStack.ExtentReports.Reporter.Configuration;

namespace UltimateQA
{
    //[SetUpFixture]
    public class SetupFixture
    {
        public string driverAlias;
        public static AventStack.ExtentReports.ExtentReports _extent;
        public static ExtentTest _test;
        public string dir = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
        public string filepath;
        public string today = DateTime.Now.ToString("yyyy-MM-dd HHmm");
        public string tick = DateTime.Now.ToString("HHmmss");

        [OneTimeSetUp]
        public void GlobalSetUp()
        {
            //Seeting up report utility               

            filepath = Path.Combine(dir , $"Test_Reports" , today);

            _extent = new AventStack.ExtentReports.ExtentReports();

            var htmlreporter = new ExtentV3HtmlReporter(Path.Combine(filepath , $"Automation_Report_{driverAlias}_{tick}.html"));            
            htmlreporter.LoadConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "extent-config.xml"));
            
            _extent.AttachReporter(htmlreporter);
            

        }

        [OneTimeTearDown]
        public void AfterClass()
        {
            try
            {
                _extent.Flush();
            }
            catch (Exception e)
            {
                throw (e);
            }

            AtataContext.Current?.CleanUp();
        }
    }
}
